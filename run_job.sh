curl --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/35238792/pipeline?ref=master" \
--form "variables[][key]=IMAGE_TAG" --form "variables[][variable_type]=env_var" --form "variables[][value]=$IMAGE_TAG" \
--form "variables[][key]=NGINX" --form "variables[][variable_type]=env_var" --form "variables[][value]=$NGINX" \
--form "variables[][key]=NAME_ARTIFACT" --form "variables[][variable_type]=env_var" --form "variables[][value]=$NAME_ARTIFACT"
PIPELINE_ID=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/35238792/pipelines" | jq '.[0] | .id')
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/35238792/pipelines/$PIPELINE_ID/jobs" | jq ' .[] | select(.name == "run_prod") | .id')
curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/35238792/jobs/$JOB_ID/play"